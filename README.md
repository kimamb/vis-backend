# Backend - QAD

Backend - "Quick and Dirty"

---

Gjøres klart på linux med:

```bash
git clone https://gitlab.com/visuelt-inventarsystem/vis-backend-qad.git
cd vis-backend-qad
wget -qO- https://downloads.tuxfamily.org/godotengine/3.1.1/Godot_v3.1.1-stable_linux_headless.64.zip | busybox unzip -
```

og kjøres med:

```bash
./Godot_v3.1.1-stable_linux_headless.64 --path .
```
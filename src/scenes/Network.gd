extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

sync func get_inventory():
	send_inventory()

func send_inventory():
	rpc("send_inventory", get_parent().get_json())

sync func new_item(serie, navn, antall, modell, location, manufacturer, description):
	print(navn + str(antall))
	$"..".new_item(serie, navn, antall, modell, location, manufacturer, description)
	
sync func del_item(serie):
	$"..".del_item(serie)

sync func change_amount_item(serie, antall):
	$"..".change_amount_item(serie, antall)

sync func  sort_inventory():
	$"..".sort_inventory()
extends Node

export var serie : String
export var navn : String
export var antall = 0.0
export var modell : String
export var location : String
export var manufacturer : String
export var description : String

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func save():
	var save_dict = {"serie": serie, "navn": navn, "antall": antall, "location" : location, "modell": modell, "manufacturer": manufacturer, "description": description}
	return save_dict
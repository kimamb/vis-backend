extends Node

var item_scene = preload("res://src/scenes/Item.tscn")
const SAVE_PATH   = "res://inventory.json"
const CONFIG_PATH = "res://config.json"

# Called when the node enters the scene tree for the first time.
func _ready():
	load_inventory()
	var network = NetworkedMultiplayerENet.new()
	network.create_server(load_port_config(), 32)
	get_tree().set_network_peer(network)
	network.connect("peer_connected", self, "_peer_connected")
	get_tree().multiplayer.connect("network_peer_packet", self, "_on_packet_recieved")
	

func _peer_connected(id):
	print("ID: " + str(id) + " connected. Total users: " + str(get_tree().get_network_connected_peers().size()) + "\n")

func _peer_disconected(id):
	print("ID: " + str(id) + " disonnected. Total users: " + str(get_tree().get_network_connected_peers().size()) + "\n")

func _on_packet_recieved(id, packet):
	var serverData : String = packet.get_string_from_utf8()
	print("ID: " + str(id) + serverData)

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		print("Quiting")
		get_tree().set_network_peer(null)
		get_tree().quit()

func new_item(serie, navn, antall, modell, location, manufacturer, description):
	var item = item_scene.instance()
	item.serie = serie
	item.navn = navn
	item.antall = antall
	item.modell = modell
	item.location = location
	item.manufacturer = manufacturer
	item.description = description
	add_child(item)
	save_inventory()

func save_inventory():
	var save_game = File.new()
	save_game.open(SAVE_PATH, File.WRITE)
	var save_nodes = get_tree().get_nodes_in_group("persistent")
	for i in save_nodes:
		var node_data = i.call("save");
		save_game.store_line(to_json(node_data))
	save_game.close()

func get_json():
	var save_game = File.new()
	save_game.open(SAVE_PATH, File.READ)
	var json : String = save_game.get_as_text()
	save_game.close()
	if(json == ""):
		json = '{"serie": serie, "antall" : 0, "navn" : "Empty database", "modell" : "", "location" : "", "manufacturer" : "", "description" : ""}'
	return json
	

func load_port_config():
	var port : int
	var save_game = File.new()
	if not save_game.file_exists(CONFIG_PATH):
		return # Error! We don't have a save to load.

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open(CONFIG_PATH, File.READ)
	while not save_game.eof_reached():
		var current_line = parse_json(save_game.get_line())
		# Firstly, we need to create the object and add it to the tree and set its position.
		port = current_line["port"]
		# Now we set the remaining variables.
	save_game.close()
	return port
	
func load_inventory():
	var save_game = File.new()
	if not save_game.file_exists(SAVE_PATH):
		return # Error! We don't have a save to load.

	# We need to revert the game state so we're not cloning objects
	# during loading. This will vary wildly depending on the needs of a
	# project, so take care with this step.
	# For our example, we will accomplish this by deleting saveable objects.
	var save_nodes = get_tree().get_nodes_in_group("persistent")
	for i in save_nodes:
		i.queue_free()

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open(SAVE_PATH, File.READ)
	while not save_game.eof_reached():
		var current_line = parse_json(save_game.get_line())
		# Firstly, we need to create the object and add it to the tree and set its position.
		if(current_line != null):
			var new_object = load("res://src/scenes/Item.tscn").instance()
			get_node("/root/Main").add_child(new_object)
			new_object.serie = current_line["serie"]
			new_object.navn = current_line["navn"]
			new_object.antall = current_line["antall"]
			new_object.modell = current_line["modell"]
			new_object.location = current_line["location"]
			new_object.manufacturer = current_line["manufacturer"]
			new_object.description = current_line["description"]
			# Now we set the remaining variables.
			for i in current_line.keys():
				if i == "filename" or i == "parent":
					continue
				new_object.set(i, current_line[i])
	save_game.close()
	
func del_item(serie):
	print("sletter " + serie) 
	var save_game = File.new()
	save_game.open(SAVE_PATH, File.WRITE)
	var save_nodes = get_tree().get_nodes_in_group("persistent")
	for i in save_nodes:
		if(i.serie != serie):
			var node_data = i.call("save");
			print(node_data)
			save_game.store_line(to_json(node_data))
	save_game.close()
	load_inventory()

func change_amount_item(serie, antall):
	var save_game = File.new()
	save_game.open(SAVE_PATH, File.WRITE)
	var save_nodes = get_tree().get_nodes_in_group("persistent")
	for i in save_nodes:
		if(i.serie == serie):
			i.antall = antall
		var node_data = i.call("save");
		save_game.store_line(to_json(node_data))
	save_game.close()
	load_inventory()

func sort_inventory():
	var array = []
	var out = File.new()

	out.open(SAVE_PATH, File.WRITE)
	
	var i = 0

	var save_nodes = get_tree().get_nodes_in_group("persistent")
	for i in save_nodes:
		var node_data = i.call("save");
		print(node_data)
		array.push_back(node_data)
		array.sort_custom(LagerSort, "sort")
	
	for i in range(0, array.size()):
		print(array[i])
		out.store_line(to_json(array[i]))
	
	out.close()
	load_inventory()

class LagerSort:
    static func sort(a, b):
        if (a["location"] + a["navn"]) < (b["location"] + b["navn"]):
            return true
        return false